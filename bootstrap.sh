#!/usr/bin/env bash
set -e

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
base_dir=${script_dir}

source ${base_dir}/paths_lib.sh

set_plat

rm -f ${base_dir}/current
ln -s ${plat} ${base_dir}/current
