#!/usr/bin/env bash
set -e

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
base=${script_dir}

function set_plat() {
    local os=`uname -s`
    plat=''

    case "$os" in
        Linux)
            plat='linux'
            ;;
        SunOS)
            echo "SunOS is not supported"
            exit 1
            ;;
        Darwin)
            plat='osx'
        ;;
        FreeBSD)
            echo "FreeBSD is not supported"
            exit 1
            ;;
    esac

    plat_path=${script_dir}/${plat}
}
