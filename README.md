#Scripts

## `./print_paths.sh`

   Print variable definitions that include the locations of the tools in the module.
   In particular, `TOOLCHAIN_PATH` is the module root, and `TOOLCHAIN_PLAT_PATH` is
   the directory for your OS.

###Usage:

```
eval $(./print_paths.sh)
echo $TUP_PATH
```
Output:
```
/home/alex/Code/stm32/modules/arm-gcc-toolchain/linux/tup-0.7.5
```

#Tools

[Gnu Embedded Toolchain](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm)

[OpenOCD](http://openocd.org/)

[Tup](http://gittup.org/tup/)

