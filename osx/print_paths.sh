#!/usr/bin/env bash
set -e

plat_path=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
echo "READLINK_PATH=${plat_path}/greadlink-8.27/greadlink"
