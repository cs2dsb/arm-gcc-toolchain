#!/usr/bin/env bash
set -e

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
base=${script_dir}

source ${script_dir}/paths_lib.sh

set_plat

echo "GCC_ARM_NONE_EABI_PATH=${plat_path}/gcc-arm-none-eabi-5_4-2016q3"
echo "TUP_PATH=${plat_path}/tup-0.7.5"
echo "OPENOCD_PATH=${plat_path}/openocd_0.10.0-201701241841"
echo "TOOLCHAIN_PATH=${base}"
echo "TOOLCHAIN_PLAT_PATH=${plat_path}"

${plat_path}/print_paths.sh
