#!/usr/bin/env bash
set -ex

script_dir=$( cd $(dirname ${BASH_SOURCE[0]}); pwd )

cd ${script_dir}/source
git submodule foreach --recursive git clean -fd
git clean -fd
