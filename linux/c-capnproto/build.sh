#!/usr/bin/env bash
set -ex

script_dir=$( cd $(dirname ${BASH_SOURCE[0]}); pwd )

# Set clean=0 on command line to skip git clean for iteration during testing
echo ${clean:=1} > /dev/null

function main() {
    init_git
    build
    copy_output
}

function init_git() {
    if [[ ${clean} -eq "1" ]]; then
        git submodule update --init --recursive source
        cd ${script_dir}/source
        git reset --hard
        git clean -fxd
    fi
}

function build() {
    cd ${script_dir}/source/
    autoreconf -f -i -s
    ./configure
    make
    make check
}

function copy_output() {
    build_dir=${script_dir}/builds/$(date -Is | tr 'T:+' '_-_')_$(hostname --fqdn)

    mkdir -p ${build_dir}/out
    cp -r ${script_dir}/source/.libs ${build_dir}/out

    cat > ${build_dir}/meta <<EOF
hostname      = $(hostname --fqdn)
time          = $(date -Is)

git commit    = $(cd ${script_dir}/source && git rev-parse HEAD)
git status    = "
$(cd ${script_dir}/source && git status)
"

gcc --version = "
$(gcc --version)
"

lsb_release -a = "
$(lsb_release -a)
"

uname -a = "
$(uname -a)
"

die = $(exit 1)

EOF

    rm -f ${script_dir}/builds/current
    ln -s $(basename ${build_dir}) ${script_dir}/builds/current
}

main
