# Cap'n Proto C compiler plugin

## Overview

Run the current version of `capnpc-c` with the wrapper script `./capnpc-c`

To build a new version of this dependency and commit it, you should just need to:

1. Checkout the right commit of the source code in the git submodule at `source/`.
1. Run `build.sh`.
1. Commit the new build and updated symlink `builds/current`

## `build.sh`

Currently this script does the following:

1. Updates the git submodule at `source/` containing the source code.
1. Use `git clean` in `source/`
1. Builds `source/`
1. Copies build output to `${build_dir}/out`, a new directory under `builds/`
1. Sets a symlink `builds/current` to point to `${build_dir}`
1. Writes `${builds_dir}/meta` with some metadata about the build (e.g. this machine's host name, the git commit)

## `clean.sh`

`git clean` working tree changes in `source/` and submodules that would otherwise leave the toolchain working tree dirty.
